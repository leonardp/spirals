#!/usr/bin/env python

import os
import cairo
import math


class Spiral:
    """ generates the points of a spiral around a centerpoint in carthesian coordinates
    """

    def __init__(
        self,
        spiral_type,
        center_x,
        center_y,
        res=0.05,
        rot=0.0,
        turns=1.0,
        radius=False,
        scale=False,
        ccw=False,
    ):
        self.center_x = center_x
        self.center_y = center_y

        self.theta = 0.0
        self.delta = res
        self.rot = math.radians(rot)
        self.turns = turns * 2 * math.pi
        self.ccw = ccw

        if radius:
            self.radius = radius
        else:
            self.radius = center_x

        if spiral_type == "archimedes":
            self.spiral_func = lambda theta, trig: self.scale * theta * trig(theta)
        elif spiral_type == "log":
            self.k = 0.8
            self.spiral_func = (
                lambda theta, trig: self.scale * math.exp(self.k * abs(theta)) * trig(theta)
            )
        elif spiral_type == "fermat":
            self.spiral_func = (
                lambda theta, trig: self.scale * math.sqrt(abs(theta)) * trig(theta)
            )
        else:
            raise NotImplemented("Unknown Spiral.")

        if scale:
            self.scale = scale
        else:
            self.set_scale()

    def get_coordinates(self, theta):
        x = self.center_x + self.spiral_func(theta, math.cos)
        y = self.center_y + self.spiral_func(theta, math.sin)
        if self.rot:
            x, y = self.rot_coordinates(x, y)
        return x, y

    def rot_coordinates(self, x, y):
        x = x - self.center_x
        y = y - self.center_y
        x_t = (x * math.cos(self.rot) - y * math.sin(self.rot)) + self.center_x
        y_t = (x * math.sin(self.rot) + y * math.cos(self.rot)) + self.center_y
        return x_t, y_t

    def set_scale(self):
        self.scale = 1.0
        x, y = self.get_coordinates(self.turns)
        norm = math.sqrt((x - self.center_x) ** 2 + (y - self.center_y) ** 2)
        self.scale = self.radius / norm
        return True

    def __next__(self):
        x, y, = self.get_coordinates(self.theta)
        if self.ccw:
            self.theta -= self.delta
        else:
            self.theta += self.delta
        return x, y

    def __iter__(self):
        return self


class SVGCreator(object):
    """ class for drawing the svg
    """

    def __init__(self, width, height, fname):
        self.center_x = width / 2
        self.center_y = height / 2

        filename = os.path.join(f"{fname}.svg")
        self.surface = cairo.SVGSurface(filename, width, height)
        self.ctx = cairo.Context(self.surface)
        self.ctx.save()

    def draw(self, spiral, curve=True):
        """ draw the spiral
        """
        self.ctx.set_source_rgb(0, 0, 0)

        self.ctx.move_to(self.center_x, self.center_y)
        self.ctx.new_sub_path()  # needed or else lines will be drawn to its origin

        while abs(spiral.theta) < spiral.turns:
            if curve:
                pts = []
                for x in range(3):
                    pts.extend(next(spiral))
                self.ctx.curve_to(*pts)
                self.ctx.stroke()
                self.ctx.move_to(pts[-2], pts[-1])

            else:
                x, y = next(spiral)
                self.ctx.line_to(x, y)
                self.ctx.stroke()
                self.ctx.move_to(x, y)

    def draw_circle(self, center_x, center_y, radius):
        self.ctx.set_source_rgb(0, 0, 0)
        self.ctx.new_sub_path()  # needed or else lines will be drawn to its origin

        self.ctx.arc(center_x, center_y, radius, 0, 2 * math.pi)
        self.ctx.stroke()

    def finalize(self):
        """ what does it all mean?
        """
        self.ctx.restore()
        self.ctx.show_page()
        self.surface.finish()


if __name__ == "__main__":

    options = {"fname": "demo/spirals", "width": 1024, "height": 1024}

    svg = SVGCreator(**options)

    spiral_opts = {
        "center_x": svg.center_x,
        "center_y": svg.center_y,
        "res": 0.01,
        "turns": 1,
    }

    spiral_opts["spiral_type"] = "archimedes"
    spiral = Spiral(**spiral_opts)
    svg.draw(spiral)

    spiral_opts["spiral_type"] = "log"
    spiral = Spiral(**spiral_opts)
    svg.draw(spiral)

    spiral_opts["spiral_type"] = "fermat"
    spiral = Spiral(**spiral_opts)
    svg.draw(spiral)

    svg.draw_circle(svg.center_x, svg.center_y, svg.center_x)

    svg.finalize()
    print("done")
