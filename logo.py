#! /usr/bin/env python

from spirals import *

options = {"fname": f"logo/logo", "width": 1024, "height": 1024}
svg = SVGCreator(**options)

def draw_triple(spiral_opts, rot_os=0):
    for rot in range(0, 360, 120):
        spiral_opts["rot"] = rot+rot_os
        spiral = Spiral(**spiral_opts)
        svg.draw(spiral)

spiral_opts = {
    "spiral_type": "fermat",
    "center_x": svg.center_x,
    "center_y": svg.center_y,
    "radius": svg.center_x-1,
    "res": 0.01,
    "turns": 0.33,
}

rot_os = 33
draw_triple(spiral_opts, rot_os)

svg.draw_circle(svg.center_x, svg.center_y, svg.center_x)

radius = 33
spiral_opts["radius"] = radius

spiral_opts["ccw"] = True
draw_triple(spiral_opts, rot_os)
svg.draw_circle(svg.center_x, svg.center_y, radius)


dist = svg.center_x / 3
trip_os = 33

for rot in range(0, 360, 120):
    x = svg.center_x + (svg.center_x-dist) * math.cos(math.radians(rot+trip_os))
    y = svg.center_y + (svg.center_y-dist) * math.sin(math.radians(rot+trip_os))
    spiral_opts["center_x"] = x
    spiral_opts["center_y"] = y
    draw_triple(spiral_opts, rot_os)
    svg.draw_circle(x, y, radius)


svg.finalize()
