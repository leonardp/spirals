#! /usr/bin/env python

from spirals import *

for s in ["archimedes", "log", "fermat"]:
    demo_name = s + "_2_rot"
    options = {"fname": f"demo/{demo_name}", "width": 512, "height": 512}
    svg = SVGCreator(**options)

    spiral_opts = {
        "spiral_type": s,
        "center_x": svg.center_x,
        "center_y": svg.center_y,
    }

    spiral = Spiral(**spiral_opts)
    svg.draw(spiral)
    spiral_opts["rot"] = 180
    spiral = Spiral(**spiral_opts)
    svg.draw(spiral)

    svg.draw_circle(svg.center_x, svg.center_y, svg.center_x)
    svg.finalize()

for s in ["archimedes", "log", "fermat"]:
    demo_name = s + "_2_ccw"
    options = {"fname": f"demo/{demo_name}", "width": 512, "height": 512}
    svg = SVGCreator(**options)

    spiral_opts = {
        "spiral_type": s,
        "center_x": svg.center_x,
        "center_y": svg.center_y,
    }

    spiral = Spiral(**spiral_opts)
    svg.draw(spiral)
    spiral_opts["ccw"] = True
    spiral = Spiral(**spiral_opts)
    svg.draw(spiral)

    svg.draw_circle(svg.center_x, svg.center_y, svg.center_x)
    svg.finalize()


for s in ["archimedes", "log", "fermat"]:
    demo_name = s + "_6"
    options = {"fname": f"demo/{demo_name}", "width": 512, "height": 512}
    svg = SVGCreator(**options)

    spiral_opts = {
        "spiral_type": s,
        "center_x": svg.center_x,
        "center_y": svg.center_y,
        "res": 0.75,
        "turns": 3,
    }
    for r in range(0, 360, 60):
        spiral_opts["rot"] = r
        spiral = Spiral(**spiral_opts)
        svg.draw(spiral, curve=False)
    svg.draw_circle(svg.center_x, svg.center_y, svg.center_x)
    svg.finalize()

for s in ["archimedes", "log", "fermat"]:
    demo_name = s + "_many"
    options = {"fname": f"demo/{demo_name}", "width": 512, "height": 512}
    svg = SVGCreator(**options)

    spiral_opts = {
        "spiral_type": s,
        "center_x": svg.center_x,
        "center_y": svg.center_y,
        "turns": 5,
        "res": 0.05,
    }
    for r in range(0, 360, 60):
        spiral_opts["center_x"] = svg.center_x + svg.center_x/2 * math.cos(math.radians(r))
        spiral_opts["center_y"] = svg.center_y + svg.center_y/2 * math.sin(math.radians(r))
        spiral_opts["radius"] = svg.center_x / 2
        for rr in range(0, 360, 60):
            spiral_opts["rot"] = rr

            spiral = Spiral(**spiral_opts)
            svg.draw(spiral)

    svg.draw_circle(svg.center_x, svg.center_y, svg.center_x)
    svg.finalize()
