#! /usr/bin/env python

import os
import multiprocessing
from spirals import *

jobs = multiprocessing.cpu_count()

svg_path = "ani_demo/svg"
png_path = "ani_demo/png"

os.system(f"rm {svg_path}/*")
os.system(f"rm {png_path}/*")

quali = 100

frames = [angle for angle in range(0,360,1)]

def ani2_step(angle):
    options = {"fname": f"{svg_path}/frame_{angle:03}", "width": 1080, "height": 1080}
    svg = SVGCreator(**options)

    spiral_opts = {
        "center_x": svg.center_x,
        "center_y": svg.center_y,
        "res": 0.01,
        "turns": 4,
    }

    for rot in range(0, 360, 10):
        spiral_opts["ccw"] = True
        spiral_opts["rot"] = rot+angle
        spiral_opts["spiral_type"] = "fermat"
        spiral = Spiral(**spiral_opts)
        svg.draw(spiral)

        spiral_opts["ccw"] = False
        spiral_opts["rot"] = rot-angle
        spiral_opts["spiral_type"] = "archimedes"
        spiral = Spiral(**spiral_opts)
        svg.draw(spiral)

    svg.draw_circle(svg.center_x, svg.center_y, svg.center_x)
    svg.finalize()
    return True

def ani1_step(angle):
    options = {"fname": f"{svg_path}/frame_{angle:03}", "width": 1080, "height": 1080}
    svg = SVGCreator(**options)

    spiral_opts = {
        "center_x": svg.center_x,
        "center_y": svg.center_y,
        "res": 0.01,
        "turns": 1,
        "radius": svg.center_x / 2,
    }

    for r in range(0, 360, 60):
        spiral_opts["center_x"] = svg.center_x + svg.center_x/2 * math.cos(math.radians(r+angle))
        spiral_opts["center_y"] = svg.center_y + svg.center_y/2 * math.sin(math.radians(r+angle))

        for rot in range(0, 360, 10):

            spiral_opts["rot"] = rot+angle
            spiral_opts["ccw"] = True
            spiral_opts["spiral_type"] = "fermat"
            spiral = Spiral(**spiral_opts)
            svg.draw(spiral)

            spiral_opts["rot"] = rot
            spiral_opts["ccw"] = False
            spiral_opts["spiral_type"] = "log"
            spiral = Spiral(**spiral_opts)
            svg.draw(spiral)

            svg.draw_circle(spiral_opts["center_x"], spiral_opts["center_y"], svg.center_x / 2)

    svg.draw_circle(svg.center_x, svg.center_y, svg.center_x)
    svg.finalize()
    return True

def convert_to_png(frameno):
    cmd = f"convert -strip -quality {quali} -flatten {svg_path}/frame_{frameno:03}.svg {png_path}/frame_{frameno:03}.png"
    os.system(cmd)

pool = multiprocessing.Pool(jobs)
print("Making Frames")
results = pool.map(ani1_step, frames)
print("Converting Frames")
results = pool.map(convert_to_png, frames)
pool.close()

print("Making Video")
cmd = f'ffmpeg -i {png_path}/frame_%03d.png -c:v libx264 -vf "fps=30,format=yuv420p" ani_demo/ani.mp4'
os.system(cmd)
