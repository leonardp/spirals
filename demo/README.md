## all in one picture with defaults
![](spirals.jpg)

## archimedes
### cw and ccw
![](archimedes_2_ccw.jpg)
### rot by 180 deg
![](archimedes_2_rot.jpg)
### low res, straight lines, rot 6 times
![](archimedes_6.jpg)
### many, offset on a circle around the center
![](archimedes_many.jpg)

## fermat
### cw and ccw
![](fermat_2_ccw.jpg)
### rot by 180 deg
![](fermat_2_rot.jpg)
### low res, straight lines, rot 6 times
![](fermat_6.jpg)
### many, offset on a circle around the center
![](fermat_many.jpg)

## log
### cw and ccw
![](log_2_ccw.jpg)
### rot by 180 deg
![](log_2_rot.jpg)
### low res, straight lines, rot 6 times
![](log_6.jpg)
### many, offset on a circle around the center
![](log_many.jpg)
