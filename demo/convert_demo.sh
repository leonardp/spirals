#! /usr/bin/env sh

for f in `fd -I -a -c never -e svg`;
do
	dir=$(dirname -- "$f")/
	base=$(basename -- "$f")
	fname="${base%.*}"
	convert -strip -quality 100 -flatten -colorspace GRAY $f $dir$fname.jpg
done
