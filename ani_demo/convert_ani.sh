#! /usr/bin/env sh

cwd=$(pwd)

cmdfile="cmds.list"
echo "" > $cmdfile

mkdir -p $cwd/png

for f in `fd -I -a -c never -e svg`;
do
	base=$(basename -- "$f")
	fname="${base%.*}"
	echo convert -strip -quality 100 -flatten $f $cwd/png/$fname.png >> $cmdfile
done

parallel -echo -j 16 < $cmdfile

ffmpeg -i png/frame_%03d.png -c:v libx264 -vf "fps=30,format=yuv420p" ani.mp4
